# azrs-tracking


пројекат из курса алати за развој софтвера 2023/24

приказани алати:
- [x] [cmake](https://gitlab.com/batpot/azrs-tracking/-/issues/1)
- [x] [git](https://gitlab.com/batpot/azrs-tracking/-/issues/2)
- [x] [gdb](https://gitlab.com/batpot/azrs-tracking/-/issues/3)
- [x] [clang-format](https://gitlab.com/batpot/azrs-tracking/-/issues/12)
- [x] [clang-tidy](https://gitlab.com/batpot/azrs-tracking/-/issues/6)
- [x] [valgrind](https://gitlab.com/batpot/azrs-tracking/-/issues/5)
- [x] [git-hook](https://gitlab.com/batpot/azrs-tracking/-/issues/4)
- [x] [conan](https://gitlab.com/batpot/azrs-tracking/-/issues/8)
- [x] [ci](https://gitlab.com/batpot/azrs-tracking/-/issues/9)
- [x] [doxygen](https://gitlab.com/batpot/azrs-tracking/-/issues/14)
- [x] [gcov](https://gitlab.com/batpot/azrs-tracking/-/issues/13)


